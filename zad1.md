# Zadanie1 

![Markdown jest lepszy niż latex](https://memegenerator.net/img/instances/54895517.jpg)

## top 3 książek

1. S. King - Cristine
1. A. Sapkowski - Wiedzmin
1. N. Angela - Mój dziadek był drzewem czereśniowym

## top 5 miast w Polsce

* Warka
* Grójec
* Radom
* Kozienice
* Cierzpięty

## Najlepsze strony internetowe

* [sladstosu.pl](https://sladstosu.pl)
* [heeeeeeeey.com](https://heeeeeeeey.com)

## Kiedyś to było pieczywo

eh panowie coraz więcej *amatorów* się pcha do zabawy hehe mam nadzieję, że przejdzie ta *nowelizacja ustawy* i __po bułki__ będzie można chodzić tylko z licencją bo serio niektórzy nie mają ani __doświadczenia ani wyobraźni__ i na przykład ~~upuszczą bułkę na podłogę~~

## latex w markdown (działa out of the box w gitlabie)

```math
I=\frac{d\Phi}{d\Omega}
```

## nagłówki w markdown to super prosta sprawa

Aby zrobić nagłówek wystarczy dodać `#` przed linią tekstu

kolejne poziomy to kolejne znaki `#`

### takie to proste

Zagnieżdżenie nagłówków jest jeszcze prostsze 